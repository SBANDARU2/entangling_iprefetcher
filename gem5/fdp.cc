/*
 * Copyright (c) 2022-2023 The University of Edinburgh
 * All rights reserved
 *
 * The license below extends only to copyright in the software and shall
 * not be construed as granting a license to any other intellectual
 * property including but not limited to intellectual property relating
 * to a hardware implementation of the functionality of the software
 * licensed hereunder.  You may use the software subject to the license
 * terms below provided that you ensure that this notice is replicated
 * unmodified and in its entirety in all distributions of the software,
 * modified or unmodified, in source code or in binary form.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#include "mem/cache/prefetch/fdp.hh"

#include <utility>

#include "debug/HWPrefetch.hh"
#include "mem/cache/base.hh"
#include "params/FetchDirectedPrefetcher.hh"

namespace gem5
{

GEM5_DEPRECATED_NAMESPACE(Prefetcher, prefetch);
namespace prefetch
{

FetchDirectedPrefetcher::FetchDirectedPrefetcher(
                                const FetchDirectedPrefetcherParams &p)
    : Base(p),
      cpu(p.cpu),
      transFunctional(p.translate_functional),
      latency(cyclesToTicks(p.latency)), cacheSnoop(true),
      stats(this)


{ 
	m_et_bb_size = 0;
	m_et_bb_head = 7; // random junk value
	m_et_num_elem = 0;
}


void
FetchDirectedPrefetcher::notifyFTQInsert(const o3::FetchTargetPtr& ft)
{
    Addr blkAddr = blockAddress(ft->startAddress());
    //notifyPfAddr(blkAddr, true);
}

int FetchDirectedPrefetcher::find_et_entry_size(Addr addr) {
   
	std::tr1::unordered_map<int, et_entry>::iterator itr;
        itr = m_et.find((addr/blkSize)%MAX_NUM_ET_ELEM);
	int size;
	if(itr != m_et.end()) {
	   if(((itr->second).src_addr) == addr) { 
           size = (itr->second).et_src_bb_size; }
	   else { 
	      size = 0;
	   }
	}
    
	return size;

}

void
FetchDirectedPrefetcher::notifyFTQRemove(const o3::FetchTargetPtr& ft)
{

}

void FetchDirectedPrefetcher::notify(const PacketPtr &pkt, const PrefetchInfo &pfi)
{
	printf("Harsha notify\n");

	int miss = pfi.isCacheMiss();
	//update size
        Addr blkAddr = pkt->getBlockAddr(blkSize);

        if((m_et_bb_head + (m_et_bb_size-1)*blkSize)!= blkAddr) { // accessing same cache line
	     if(((m_et_bb_head + m_et_bb_size*blkSize )==blkAddr)&& (m_et_bb_size <64)){
	     	m_et_bb_size++;
              	printf(" new line added to basic block with head=0x%x and current address=0x%x and new size=%d \n",  m_et_bb_head, blkAddr, m_et_bb_size);
	     }
	     else  
	     {
	        
		// start of new basic block. That is end of previous basic block
                //create entry in entabled table
		if(m_et_bb_size !=0){
                      m_et.insert(std::make_pair((m_et_bb_head/blkSize)%MAX_NUM_ET_ELEM, et_entry(m_et_bb_head, m_et_bb_size, 0,0,0,0,0,0,0))); 
		      printf("ET entry added\n");
		}

                Tick t = curTick();
	     	if(m_hb.size() ==  MAX_NUM_HB_ELEM)
                     {
	               printf("History Buffer Full : Popping the first one. Size = %d\n", m_hb.size());
	     	  m_hb.pop_front();
	     	}
		if(m_et_bb_size !=0){
                     m_hb.push_back(hist_buff_elem(m_et_bb_head, m_head_tick, m_et_bb_size));
	     	//update history buffer before starting a new basic block 
	     	printf(" hist buffer updated with bb head = 0x%x and bb size = %d, history buffer size = %d\n", m_et_bb_head, m_et_bb_size, m_hb.size());
	     	}
	     	//Harsha: how do i push them into a queue(hist buffer) and maintain that queue now ??? 
	     	m_et_bb_head = blkAddr;
	     	m_et_bb_size = 1;
                m_head_tick = t;
              	printf(" new  basic block with head=0x%x and current address=0x%x and new size=%d Time = %d\n",  m_et_bb_head, blkAddr, m_et_bb_size, m_head_tick);

		// on Hit access ET table
		 // iterating over all value of umap
		std::tr1::unordered_map<int, et_entry>::iterator itr_src, itr_dst1, itr_dst2, itr_dst3, itr_dst4, itr_dst5, itr_dst6;
                if(!miss) {
		     
		       printf("BB Hit seen\n");	
			itr_src = m_et.find((m_et_bb_head/blkSize)%MAX_NUM_ET_ELEM);
		        if(itr_src != m_et.end()) {
			      printf("BB Hit Seen : Looking for BB's fetches in ET\n");
			      if((((itr_src->second).src_addr) == m_et_bb_head) && ((itr_src->second).num_dst!=0)) { 
			      Addr src_addr = (itr_src->second).src_addr;
			      Addr dst_addr1 = (itr_src->second).dst_addr1;
			      int dst1_size = find_et_entry_size(dst_addr1);
			      Addr dst_addr2 = (itr_src->second).dst_addr2;
			      int dst2_size = find_et_entry_size(dst_addr2);
			      Addr dst_addr3 = (itr_src->second).dst_addr3;
			      int dst3_size = find_et_entry_size(dst_addr3);
			      Addr dst_addr4 = (itr_src->second).dst_addr4;
			      int dst4_size = find_et_entry_size(dst_addr4);
			      Addr dst_addr5 = (itr_src->second).dst_addr5;
			      int dst5_size = find_et_entry_size(dst_addr5);
			      Addr dst_addr6 = (itr_src->second).dst_addr6;
			      int dst6_size = find_et_entry_size(dst_addr6);
			      int src_size = (itr_src->second).et_src_bb_size;
			      int num_dst = (itr_src->second).num_dst;
                          
			      printf("src_size = %d src_addr = 0x%x num_dst = %d\n", src_size, src_addr, num_dst);
			      
			      for(int i=0; i<src_size; i++){notifyPfAddr(src_addr + i*blkSize, false);}
                              if(num_dst > 0) {
			         printf("dst1_size = %d dst1_addr = 0x%x\n", dst1_size, dst_addr1);
			         for(int i=0; i<dst1_size; i++){notifyPfAddr(dst_addr1 + i*blkSize, false);}}
                              if(num_dst > 1) {
			         printf("dst2_size = %d dst2_addr = 0x%x\n", dst2_size, dst_addr2);
			         for(int i=0; i<dst2_size; i++){notifyPfAddr(dst_addr2 + i*blkSize, false);}}
                              if(num_dst > 2) {
			         printf("dst3_size = %d dst3_addr = 0x%x\n", dst3_size, dst_addr3);
			         for(int i=0; i<dst3_size; i++){notifyPfAddr(dst_addr3 + i*blkSize, false);}}
                              if(num_dst > 3) {
			         printf("dst4_size = %d dst4_addr = 0x%x\n", dst4_size, dst_addr4);
			         for(int i=0; i<dst4_size; i++){notifyPfAddr(dst_addr4 + i*blkSize, false);}}
                              if(num_dst > 4) {
			         printf("dst5_size = %d dst5_addr = 0x%x\n", dst5_size, dst_addr5);
			         for(int i=0; i<dst5_size; i++){notifyPfAddr(dst_addr5 + i*blkSize, false);}}
                              if(num_dst > 5) {
			         printf("dst6_size = %d dst6_addr = 0x%x\n", dst6_size, dst_addr6);
			         for(int i=0; i<dst6_size; i++){notifyPfAddr(dst_addr6 + i*blkSize, false);}}

			   }

			}
		}
		if(miss){
			printf("BB Miss Seen\n");
		}
	     }
	}
        
        
       	
	//, update head , history buffer 
}
void FetchDirectedPrefetcher::notifyFill(const PacketPtr &pkt)

{
    Tick t = curTick();
    Addr blkAddr = pkt->getBlockAddr(blkSize);
    printf("Harsha notify fill. Addr = 0x%x\n", blkAddr);
    int start_time;
    int latency;
    int found=0;
    int src_entangling_time;

    std::list<hist_buff_elem>::iterator it;
    for(it = m_hb.begin(); it!=m_hb.end(); it++)
    {
       if(((*it).m_hb_bb_head) ==  blkAddr) {
           
         start_time = (*it).m_hb_timestamp;
	 found = 1;
       }
    }

    if(found){
       latency = t - start_time;
       printf("Searching in History Table latency = %d ticks before. t = %d start_time = %d\n",latency, t, start_time);
       src_entangling_time = start_time - latency;
       std::list<hist_buff_elem>::iterator itr;
        for( itr = m_hb.begin(); next(itr,1)!=m_hb.end(); itr++) {
		printf("finding src in HB\n");
	   
             if(((*itr).m_hb_timestamp < src_entangling_time) && ((*(std::next(itr,1))).m_hb_timestamp > src_entangling_time)) {
	     
	        Addr src_addr = (*itr).m_hb_bb_head;
	        std::tr1::unordered_map<int, et_entry>::iterator it_src;
		printf("finding src in ET\n");
                it_src = m_et.find((src_addr/blkSize)%MAX_NUM_ET_ELEM);
		if(it_src != m_et.end()) {
		    if((it_src->second).src_addr == src_addr) {
	                printf("Found src. Entangled : src_addr = 0x%x, dst_addr = 0x%x\n", src_addr, blkAddr);
		        if((it_src->second).num_dst == 0) {(it_src->second).dst_addr1 = blkAddr; (it_src->second).num_dst++; break;}
		        if((it_src->second).num_dst == 1) {(it_src->second).dst_addr2 = blkAddr; (it_src->second).num_dst++; break;}
		        if((it_src->second).num_dst == 2) {(it_src->second).dst_addr3 = blkAddr; (it_src->second).num_dst++; break;}
		        if((it_src->second).num_dst == 3) {(it_src->second).dst_addr4 = blkAddr; (it_src->second).num_dst++; break;}
		        if((it_src->second).num_dst == 4) {(it_src->second).dst_addr5 = blkAddr; (it_src->second).num_dst++; break;}
		        if((it_src->second).num_dst == 5) {(it_src->second).dst_addr6 = blkAddr; (it_src->second).num_dst++; break;}
		        if((it_src->second).num_dst == 6) {
				(it_src->second).dst_addr6 = (it_src->second).dst_addr5; 
				(it_src->second).dst_addr5 = (it_src->second).dst_addr4; 
				(it_src->second).dst_addr4 = (it_src->second).dst_addr3; 
				(it_src->second).dst_addr3 = (it_src->second).dst_addr2; 
				(it_src->second).dst_addr2 = (it_src->second).dst_addr1; 
				(it_src->second).dst_addr1 = blkAddr; 
				
				/*(it_src->second).num_dst++;*/ break;}
	            }
		}
	     } 
              
	}
         

    }
    
}

void
FetchDirectedPrefetcher::notifyPfAddr(Addr addr, bool virtual_addr)
{
   printf("Harsha FDP\n");
    Addr blk_addr = blockAddress(addr);

    // Check if the address is already in the prefetch queue
    std::list<PFQEntry>::iterator it = std::find(pfq.begin(),
                                pfq.end(), blk_addr);
    if (it != pfq.end()) {
        DPRINTF(HWPrefetch, "%#x already in prefetch_queue\n", blk_addr);
        return;
    }

    stats.pfIdentified++;

    // Create the packet for this address
    PacketPtr pkt = createPrefetchPacket(blk_addr, virtual_addr);

    if (!pkt) {
        DPRINTF(HWPrefetch, "Fail to create packet\n");
        delete pkt;
        return;
    }

    stats.pfPacketsCreated++;

    if (cacheSnoop && (inCache(pkt->getAddr(), pkt->isSecure())
                || (inMissQueue(pkt->getAddr(), pkt->isSecure())))) {
        stats.pfInCache++;
        DPRINTF(HWPrefetch, "Drop Packet. In Cache / MSHR\n");
        return;
    }

    Tick t = curTick() + latency;
    DPRINTF(HWPrefetch, "Addr: %#x Add packet to PFQ. pkt PA:%#x, PFQ sz:%i\n",
                        blk_addr, pkt->getAddr(), pfq.size());

    stats.pfCandidatesAdded++;
    if(pfq.size() < MAX_NUM_PQ_ELEM)
        pfq.push_back(PFQEntry(blk_addr, pkt, t));
}


RequestPtr
FetchDirectedPrefetcher::createPrefetchRequest(Addr vaddr)
{
    RequestPtr req = std::make_shared<Request>(
            vaddr, blkSize, 0, requestorId, vaddr, 0);
    req->setFlags(Request::PREFETCH);
    return req;
}


PacketPtr
FetchDirectedPrefetcher::createPrefetchPacket(Addr addr, bool virtual_addr)
{
    /* Create a prefetch memory request */
    RequestPtr req = nullptr;
    Flags flags = Request::INST_FETCH|Request::PREFETCH;

    if (virtual_addr) {
        // The address is virtual -> we need translate first
        req = std::make_shared<Request>(
                addr, blkSize, flags, requestorId, addr, 0);


        // Translate the address from virtual to physical using
        // the functional translation function.
        // Note: This of course a hack and is not how it is in a real system.
        // Using functional tranlation underestimate the latency of the
        // translation and the page walk.
        // TODO: Add timing translation!
        if (!translateFunctional(req)) {
            return nullptr;
        }

    } else {
        // The adddress is physical -> no translation needed.
        req = std::make_shared<Request>(
                addr, blkSize, flags, requestorId);
    }

    if (req->isUncacheable()) {
        return nullptr;
    }

    req->taskId(context_switch_task_id::Prefetcher);
    PacketPtr pkt = new Packet(req, MemCmd::HardPFReq);
    pkt->allocate();

    return pkt;
}


bool
FetchDirectedPrefetcher::translateFunctional(RequestPtr req)
{
    if (mmu == nullptr) {
        return false;
    }

    auto tc = cache->system->threads[req->contextId()];

    DPRINTF(HWPrefetch, "%s Try trans of pc %#x\n",
                                mmu->name(), req->getVaddr());
    Fault fault = mmu->translateFunctional(req, tc, BaseMMU::Read);
    if (fault == NoFault) {
        DPRINTF(HWPrefetch, "%s Translation of vaddr %#x succeeded: "
                        "paddr %#x \n", mmu->name(), req->getVaddr(),
                        req->getPaddr());

        stats.translationSuccess++;
        return true;
    }
    stats.translationFail++;
    return false;
}


PacketPtr
FetchDirectedPrefetcher::getPacket()
{
    if (pfq.size() == 0)
    {
        return nullptr;
    }
    PacketPtr pkt = pfq.front().pkt;

    DPRINTF(HWPrefetch, "Issue Prefetch to: pkt:%#x, PC:%#x, PFQ size:%i\n",
                        pkt->getAddr(), pfq.front().addr, pfq.size());

    pfq.pop_front();

    prefetchStats.pfIssued++;
    issuedPrefetches++;
    return pkt;
}


void
FetchDirectedPrefetcher::regProbeListeners()
{
    Base::regProbeListeners();

    if (cpu == nullptr) {
        warn("No CPU to listen from registered\n");
        return;
    }
    typedef ProbeListenerArgFunc<o3::FetchTargetPtr> FetchTargetListener;
    listeners.push_back(
            new FetchTargetListener(cpu->getProbeManager(), "FTQInsert",
                [this](const o3::FetchTargetPtr &ft)
                    { notifyFTQInsert(ft); }));

    listeners.push_back(
            new FetchTargetListener(cpu->getProbeManager(), "FTQRemove",
                [this](const o3::FetchTargetPtr &ft)
                    { notifyFTQRemove(ft); }));

}


FetchDirectedPrefetcher::Stats::Stats(statistics::Group *parent)
    : statistics::Group(parent),
    ADD_STAT(fdipInsertions, statistics::units::Count::get(),
            "Number of notifications from an insertion in the FTQ"),
    ADD_STAT(pfIdentified, statistics::units::Count::get(),
            "number of prefetches identified."),
    ADD_STAT(pfInCache, statistics::units::Count::get(),
            "number of prefetches hit in in cache"),
    ADD_STAT(pfInCachePrefetched, statistics::units::Count::get(),
            "number of prefetches hit in cache but prefetched"),
    ADD_STAT(pfPacketsCreated, statistics::units::Count::get(),
            "number of prefetch packets created"),
    ADD_STAT(pfCandidatesAdded, statistics::units::Count::get(),
            "Number of perfetch candidates added to the prefetch queue"),
    ADD_STAT(translationFail, statistics::units::Count::get(),
             "Number of prefetches that failed translation"),
    ADD_STAT(translationSuccess, statistics::units::Count::get(),
             "Number of prefetches that succeeded translation")
{
}

} // namespace prefetch
} // namespace gem5
