//64B cache line
//ET table is made direct mapped for simplicity . ET_DEPTH = NUM_SETS_ET*NUM_WAYS_ET = 128*16 = 2048
//ET size : ET_DEPTHXET_WIDTH
//I cache is 32KB , 8 way (4 hit cycles) : NUM_SETS = 64 NUM_WAYS_PER_SET = 8, WIDTH = 52(tag) + 11(et index) + 1(access bit))
module entangled_prefetcher_top #(parameter ADDRESS_WIDTH = 64, HBUF_WIDTH=(ADDRESS_WIDTH-6+12+6), HBUF_DEPTH=16, ET_DEPTH = 2048, ET_WIDTH = (6+63 + ADDRESS_WIDTH -6), ICACHE_NUM_SETS = 64, ICACHE_NUM_WAYS = 8, ICACHE_WIDTH = (52 + 11 + 1), PQ_DEPTH = 32, PQ_WIDTH = (11 + 12 + 1 + ADDRESS_WIDTH-6) )(
input  [ADDRESS_WIDTH-1:0] address,
input  valid, 
input  l2_valid,
input  clk,
input  rstn,
output logic ready,
//et
output logic et_read_write,
output logic [$clog2(ET_DEPTH)-1:0] et_index,
input  [ET_WIDTH-1:0] et_rd_data,
output logic [ET_WIDTH-1:0] et_wr_data,
output logic et_enable,
//cache
output logic icache_read_write,
output logic [$clog2(ICACHE_NUM_SETS)-1:0] icache_set_index,
output logic [ICACHE_NUM_WAYS-1 : 0] icache_enable,
input  [ICACHE_WIDTH*ICACHE_NUM_WAYS-1:0] icache_set_rd_data,
output logic [ICACHE_WIDTH-1 : 0] icache_wr_data
);


reg [ADDRESS_WIDTH-1-6:0] head;
reg [5:0] size;
reg [11:0] timestamp_start_of_block_access;
reg [19:0] timer;
reg [5:0] size_counter, n_size_counter;


// determine whether a cache line is head of a basic block or not
wire is_block_head;

//cache
wire cache_hit;
wire [ICACHE_NUM_WAYS-1 : 0] cache_tag_compare;

//PQ - Prefetch queue
reg [$clog2(PQ_DEPTH)-1 : 0] n_pq_wr_ptr;
reg [$clog2(PQ_DEPTH)-1 : 0] pq_wr_ptr;
reg [$clog2(PQ_DEPTH)-1 : 0] n_pq_rd_ptr;
reg [$clog2(PQ_DEPTH)-1 : 0] pq_rd_ptr;
reg [PQ_WIDTH - 1 : 0] pq_wr_data;
logic [PQ_WIDTH - 1 : 0] pq_rd_data;
reg [PQ_WIDTH-1:0] prefetch_queue[PQ_DEPTH];
wire pq_full;
wire pq_empty;

//ET table read fsm
typedef enum reg [1:0] {IDLE, ET_READ_SRC, ET_READ_DST} state;
state next_state, current_state;
reg [ET_WIDTH-1 : 0] et_rd_data_ff;
wire [2:0] mode;
wire [1:0] confidence;
wire [5:0] src_size;
wire [ADDRESS_WIDTH-6-1:0] src_address;
wire [ET_WIDTH-3-2-ADDRESS_WIDTH-6-1:0] dst_address;
reg [5:0] dst;
reg et_rd_data_ff_en;
reg [10:0] et_index_ff;


//history buffer, width = {address(58-6),timestamp(12), size(6)}
typedef struct packed{
   logic [ADDRESS_WIDTH-1-6:0] head_address;
   logic [11:0] timestamp;
   logic [5:0] block_size;
} history_elem;

history_elem [HBUF_DEPTH-1:0] history_buf;
reg [$clog2(HBUF_DEPTH)-1:0] hbuf_wr_ptr;
wire [HBUF_DEPTH-1:0] merge_blocks;
logic [HBUF_DEPTH-1:0] merge_blocks_select;
wire [HBUF_DEPTH-1:0] overlapping_blocks;
wire [HBUF_DEPTH-1:0] consecutive_blocks;
wire [HBUF_DEPTH-1:0] [6:0] overlapping_size; // extra bit to handle overflow
logic [$clog2(HBUF_DEPTH):0] hbuf_min_diff; // it should hold HBUF_DEPTH since this is used to find the latest merge selected block

//Assume cache line is 64 bytes
//Assume back to back access to same cache line is not handled via l1I
//basic block detector
always@(posedge clk)
begin
    if(!rstn)
    begin
        head <= '0;
        size <= '0;
    end else begin
        if(valid) begin
            if(is_block_head) begin
                head <= address[ADDRESS_WIDTH-1:6];  //start of the new basic block
                size <= 6'h0;
            end
            else begin
                   size <= size + 1; // next consecutive cache line
            end  
        end
    end
end

assign is_block_head = valid && ((head[ADDRESS_WIDTH-1-6 : 0] + size + 1) != (address[ADDRESS_WIDTH-1:6]));

//do cache lookup if its a basic block

//add to history buffer if its a head
genvar i;
generate
for(i=0; i<HBUF_DEPTH; i++)
begin : history_buffer
    
    //(merge blocks if consecutive addresses or overlapping addresses) and combined size < 64 cache lines

    assign consecutive_blocks[i] = (((size + history_buf[i].block_size)<64) && (((history_buf[i].head_address - head - size) == 1) || ((head - history_buf[i].head_address - history_buf[i].block_size) == 1)));
    
    assign merge_blocks[i] = consecutive_blocks[i] || overlapping_blocks[i];
   
   //handling different overlapping cases
    assign overlapping_blocks[i] = (overlapping_size[i]<(size + history_buf[i].block_size)) && (overlapping_size[i]<64);

    assign overlapping_size[i] = size + history_buf[i].block_size - (((head < (history_buf[i].head_address + history_buf[i].block_size)) && ((head + size) > (history_buf[i].head_address + history_buf[i].block_size))) ? (history_buf[i].head_address+history_buf[i].block_size - head) : (((history_buf[i].head_address <(head + size)) && ((history_buf[i].head_address+history_buf[i].block_size)>(head+size))) ? (head + size -  history_buf[i].head_address) : 6'h0));

    always@(posedge clk)
    begin
        if(!rstn)
        begin
            history_buf[i] <= '0;
        end else begin
            if((~|merge_blocks) && (hbuf_wr_ptr == i))
            begin
                history_buf[i] <= '{head, timestamp_start_of_block_access, size}; // packed structure
            end else if(merge_blocks_select[i])
            begin
                history_buf[i].block_size <= overlapping_size[i];
                if(head < history_buf[i].head_address)
                    history_buf[i].head_address <= head;
            end
        end
    end
end
endgenerate


//history buffer write pointer
always@(posedge clk)
begin
    if(!rstn)   
    begin
        hbuf_wr_ptr <= '0;
    end else begin
        if(is_block_head)
        begin
            if(~|merge_blocks)
            begin
                hbuf_wr_ptr <= hbuf_wr_ptr + 1;
            end
        end
    end
end

//timer
always@(posedge clk)
begin
    if(!rstn)
    begin
        timer <= 20'h0;
        timestamp_start_of_block_access <= 12'h0;
    end else begin
        timer <= timer + 1;
        if(is_block_head)
            timestamp_start_of_block_access <= timer[11:0];
    end
end

always@(*) begin
   hbuf_min_diff = HBUF_DEPTH;
   merge_blocks_select = '0;
   for(int i=HBUF_DEPTH-1; i>=0; i--)
   begin
       if((((hbuf_wr_ptr > i) ? (hbuf_wr_ptr - i) : (HBUF_DEPTH-hbuf_wr_ptr+i)) < (hbuf_min_diff)) && merge_blocks[i])begin
           hbuf_min_diff = (hbuf_wr_ptr > i) ? (hbuf_wr_ptr-i) : (HBUF_DEPTH-hbuf_wr_ptr+i);
           merge_blocks_select[i] = 1'b1;
        end
   end
end

//cache hit
genvar j;
generate
for(j=0; j<ICACHE_NUM_WAYS; j++)
begin : icache_tag_compare_loop
    assign cache_tag_compare[j] = (icache_set_rd_data[j*ICACHE_WIDTH+:52] == address[12+:52]); // 52 bits tag
end
endgenerate
assign cache_hit = valid && |cache_tag_compare;


//entangled table write, read
//read is done on a basic block head cache hit
//read src basic blocks and dst basic blocks and keep them in PQ(Prefetch queue)
//write is done on a completion of basic block head
//et table read write fsm

always@(*)
begin
    next_state = current_state;
    et_read_write = is_block_head;
    et_enable = is_block_head;
    et_rd_data_ff_en = 1'b0;
    et_wr_data = {head, size, 63'h0};
    n_pq_wr_ptr = pq_wr_ptr;
    et_index = head[10:0];
    case(current_state)
        IDLE :
        begin
              if(cache_hit)
              begin
                  next_state = ET_READ_SRC; // busy in reading
                  et_read_write = 1'b0;
                  et_enable = 1'b1;
                  et_index = address[6+:11];
                  et_rd_data_ff_en = 1'b1;
              end
        end

        ET_READ_SRC :
        begin
            if(!pq_full)
            begin
                pq_wr_data = {timer[11:0], et_index_ff,1'b0,src_address + size_counter}; 
                n_pq_wr_ptr = pq_wr_ptr + 1;
                if(size_counter == src_size - 1)
                begin
                    n_size_counter = '0;
                    next_state = ET_READ_DST;
                end else begin
                    n_size_counter = size_counter + 1;
                end
            end
        end
        ET_READ_DST :
        begin
            if(!pq_full)
            begin
                pq_wr_data = {timer[11:0],et_index_ff, 1'b0,dst_address + size_counter}; 
                n_pq_wr_ptr = pq_wr_ptr + 1;
                if(size_counter == src_size - 1)
                begin
                    n_size_counter = '0;
                    next_state = IDLE;
                end else begin
                    n_size_counter = size_counter + 1;
                end
            end
        end
    endcase
end

always@(posedge clk)
begin
    if(!rstn)
    begin
        et_rd_data_ff <= '0;
        current_state <= IDLE;
        size_counter <= '0;
        et_index_ff <= '0;
    end else begin
        if(et_rd_data_ff_en) begin
           et_rd_data_ff <= et_rd_data;
           et_index_ff <= et_index;
        end
        current_state <= next_state;
        size_counter <= n_size_counter;
    end
end

always@(*)
begin
    case(mode)
        3'b000 : dst = '0;
        3'b001 : dst = 6'h1;
        3'b010 : dst = 6'h3;
        3'b011 : dst = 6'h7;
        3'b100 : dst = 6'hf;
        3'b101 : dst = 6'h1f;
        3'b110 : dst = 6'h3f;
        default : dst = 6'h0;
    endcase
end
assign mode = et_rd_data_ff[(ADDRESS_WIDTH-6 + 2) +: 3];
assign dst_address = et_rd_data_ff[ADDRESS_WIDTH-6:0];
assign confidence = et_rd_data_ff[(ADDRESS_WIDTH-6) +: 2];
assign src_address = et_rd_data_ff[(ADDRESS_WIDTH-6+2+3+6)+:(ADDRESS_WIDTH-6)];
assign src_size = et_rd_data_ff[(ADDRESS_WIDTH-6 + 2 + 3) +: 6];

//PQ
genvar k;
generate
for(k=0; k<PQ_DEPTH; k++)
begin : pq_depth
    always@(posedge clk)
    begin
        if(!rstn)
        begin
            prefetch_queue[k] <= '0;
        end else begin
            if(pq_wr_ptr == k)
               prefetch_queue[k] <= pq_wr_data; 
        end
    end
end
endgenerate

assign pq_rd_data = prefetch_queue[pq_rd_ptr];

always@(posedge clk)
begin
    if(!rstn)
    begin
        pq_rd_ptr <= '0;
        pq_wr_ptr <= '0;
    end else begin
        pq_rd_ptr <= n_pq_rd_ptr;
        pq_wr_ptr <= n_pq_wr_ptr;
    end
end

assign pq_full = ((pq_wr_ptr - pq_rd_ptr)==(PQ_DEPTH-1));
assign pq_empty = ((pq_wr_ptr - pq_rd_ptr)==0);

always@(*)
begin
    n_pq_rd_ptr = pq_rd_ptr;
    icache_read_write = 1'b0;
    icache_enable = 1'b0;
    icache_set_index = '0;
    icache_wr_data = '0;
    if(valid)
    begin
        icache_enable = 1'b1;
        icache_set_index = address[6+:$clog2(ICACHE_NUM_SETS)];
    end else if(l2_valid) begin 
        icache_read_write = 1'b1;
        icache_enable = 1'b1;
        icache_set_index = address[6+:$clog2(ICACHE_NUM_SETS)];
        icache_wr_data ={address[ADDRESS_WIDTH-1:6]}; 
    end else if(!pq_empty)
    begin
        icache_read_write = 1'b1;
        icache_enable = 1'b1;
        n_pq_rd_ptr = pq_rd_ptr + 1;
        icache_set_index = pq_rd_data[6+:$clog2(ICACHE_NUM_SETS)];
        icache_wr_data ={ pq_rd_data[6+:ICACHE_WIDTH]}; 
    end
end

assign ready = 1'b1;

endmodule
